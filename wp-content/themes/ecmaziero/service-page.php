<?php
/**
 * Template Name: Serviços
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <header class="main-header">
        <?php the_title( '<h1 class="main-title col-full">', '</h1>' ); ?>
      </header><!-- .main-header -->

      <div class="main-content service">
        <?php the_content(); ?>
      </div><!-- .main-content -->

    </article>
  <?php endwhile; ?>
</main>

<?php get_footer(); ?>
