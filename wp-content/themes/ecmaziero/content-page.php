<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="main-header">
		<?php the_title( '<h1 class="main-title">', '</h1>' ); ?>
	</header><!-- .main-header -->

	<div class="main-content">
		<?php the_content(); ?>
	</div><!-- .main-content -->

	<?php // edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="main-footer"><span class="edit-link">', '</span></footer><!-- .main-footer -->' ); ?>

</article><!-- #post-## -->
