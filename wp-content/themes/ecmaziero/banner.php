
  <div id="banner" class="site-banner">
    <div class="banner-content">
      <?php 
        if (is_front_page()) {
          $loop = CFS()->get('carousel-loop');
          foreach ( $loop as $item ) {
            // echo '<img src="'. $item['carousel-image'] .'">';

            $image = $item["carousel-image"];
            $link = $item["carousel-link"];
            
            if ($link) {echo '<a href="'. $link .'">';}
              echo '<img src="'. $image .'">';
            if ($link) {echo '</a>';}
          } 

        } else {
          the_post_thumbnail( "full" ); 
        }
      ?>
    </div>
  </div>