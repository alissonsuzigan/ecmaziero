<div class="sidebar">

  <div id="tax-schedule" class="tax-schedule">
    <div class="tax-content">

      <h2 class="tax-title">
        <a href="<?php bloginfo('url'); ?>/agenda-tributaria/">Agenda tributária</a>
      </h2>
      <span class="tax-text">
        <a href="<?php bloginfo('url'); ?>/agenda-tributaria/">Clique para acessar a angenda desse mês</a>
      </span>

    </div>
  </div>

  <div id="prices" class="prices">

    <div class="prices-content">
      
      <div id="indication" class="indication hidex">
        <h2 class="title">Indicações</h2>
        <script type="text/javascript" src="http://www.debit.com.br/resumogratuito.php?info=diversos"></script>
      </div>
      

      <div id="quotation" class="quotation">
        <?php 
          include_once("Quotation.php"); 
          $dolar = Quotation::dolar();
          $euro = Quotation::euro();
          $bovespa = Quotation::bovespa();
          $getDate = Quotation::getDate();
        ?>
        <h2 class="title">Cotações</h2>
        <table>
          <thead>
            <tr>
              <th>Moeda</th>
              <th>Valor</th> 
              <th>Variação</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Dólar</td>
              <td>R$ <?php echo number_format($dolar['quotation'], 2, ',', '.'); ?></td> 
              <td class="<?php echo $dolar['status'] ?>"><?php echo $dolar['variation'] ?></td>
            </tr>
            <tr>
              <td>Euro</td>
              <td>R$ <?php echo number_format($euro['quotation'], 2, ',', '.'); ?></td> 
              <td class="<?php echo $euro['status'] ?>"><?php echo $euro['variation'] ?></td>
            </tr>
            <tr>
              <td>Bovespa</td>
              <td><?php echo number_format($bovespa['quotation'], 0, ',', '.'); ?></td> 
              <td class="<?php echo $bovespa['status'] ?>"><?php echo $bovespa['variation'] ?></td>
            </tr>
          </tbody>
        </table>
        <p class="quotation-update">Última atualização: <?php echo $getDate; ?></p>
      </div>

    </div>

  </div>
</div>