<?php if ( has_nav_menu( 'primary' ) ) : ?>
  <nav id="menu" class="site-menu" role="navigation">
    <?php
      // Primary navigation menu.
      wp_nav_menu( array(
        'theme_location'  => 'primary',
        'menu_id'         => 'menu-list',
        'menu_class'      => 'menu-list',
        'container'       => '',
      ) );
    ?>
  </nav>
<?php endif; ?>
