<?php
/**
 * Template Name: Quem Somos
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <header class="main-header">
        <?php the_title( '<h1 class="main-title">', '</h1>' ); ?>
      </header>

      <div class="main-content">
        <?php the_content(); ?>
      </div>

    </article>
  <?php endwhile; ?>
</main>

<?php get_footer(); ?>


