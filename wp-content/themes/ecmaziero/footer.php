<?php
/**
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	 	</div><!-- .content-width -->
  </div><!-- .site-content -->

  <!-- FOOTER -->
  <footer id="footer" class="site-footer" role="contentinfo">
    <div class="content-width">
    
      <div class="footer-list">
        <div class="footer-item footer-local">
          <i class="footer-icon"></i>
          <span class="footer-text"><?php echo get_post_custom_values("contato-endereco", 23)[0]; ?></span>
        </div>

        <div class="footer-item footer-tel">
          <i class="footer-icon"></i>
          <span class="footer-text"><?php echo get_post_custom_values("contato-fone", 23)[0]; ?></span>
        </div>

        <div class="footer-item footer-email">
          <i class="footer-icon"></i>
          <span class="footer-text"><?php echo get_post_custom_values("contato-email", 23)[0]; ?></span>
        </div>
      </div>

    </div>
	</footer>
</div><!-- .page -->

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/vendor/jquery/jquery-2.0.3.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/global.min.js"></script>

<?php //if (is_home()) { ?>
<?php if (is_front_page()) { ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/carousel.min.js"></script>
<?php } ?>

</body>
</html>
