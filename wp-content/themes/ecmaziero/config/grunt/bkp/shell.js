// Shell definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    environment: {
      command: [

        // Copy the project's pre-commit hook into .git/hooks
        "cp config/git-hooks/pre-commit .git/hooks/",

        // Install gems
        "cd config",
        "bundle"
      ].join("&&"),
      options: {
        stdout: true
      }
    },
    clearStage : {
      command: ["git checkout . && git clean -f -dd"]
    },
    pullStaging : {
      command: [
        "cd /app/walmart-frontend/cms_staging/",
        "git checkout . && git clean -f -d",
        "git pull --rebase origin master"
      ].join("&&"),
      options: {
        stdout: true
      }
    },
    pullSandbox : {
      command: [
        "cd /app/walmart-frontend/cms_sandbox/",
        "git checkout . && git clean -f -d",
        "git pull --rebase origin master"
      ].join("&&"),
      options: {
        stdout: true
      }
    },
    pullProduction : {
      command: [
        "cd /app/walmart-frontend/cms_production/",
        "git checkout . && git clean -f -d",
        "git pull --rebase origin master"
      ].join("&&"),
      options: {
        stdout: true
      }
    }
  }
};