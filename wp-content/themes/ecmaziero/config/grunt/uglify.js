// Uglify definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    startup: {
      files: {
        "static/script/startup.min.js": [
          "<%= concat.startup.dest %>"
        ]
      }
    },
    global: {
      files: {
        "static/script/global.min.js": [
          "<%= concat.global.dest %>"
        ]
      }
    },
    carousel: {
      files: {
        "static/script/carousel.min.js": [
          "<%= concat.carousel.dest %>"
        ]
      }
    },

  }
};