// Watch definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    scripts: {
      files: [
        "<%= concat.startup.src %>",
        "<%= concat.global.src %>",
        "<%= concat.carousel.src %>"
      ],
      tasks: [
        "concat:startup",
        "concat:global",
        "concat:carousel",
        "uglify:startup",
        "uglify:global",
        "uglify:carousel"
      ]
    },
    sass: {
      files: [
        "static/style/sass/**/*.scss"
      ],
      tasks: [
        "compass",
        "cssmin"
      ]
    }
  }
};