// Concat definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    startup: {
      src: options.settings.startup.scripts,
      dest: "static/script/startup.js"
    },
    global: {
      src: options.settings.global.scripts,
      dest: "static/script/global.js"
    },
    carousel: {
      src: options.settings.carousel.scripts,
      dest: "static/script/carousel.js"
    }
  }
};