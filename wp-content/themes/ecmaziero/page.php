<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="col-full">
  <main id="main" class="site-main" role="main">
    <?php
      while ( have_posts() ) : the_post();
        get_template_part( 'content', 'page' );
      endwhile;
    ?>
  </main>
</div>

<?php get_footer(); ?>
