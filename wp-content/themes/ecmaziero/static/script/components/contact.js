app.contact = (function() {
  "use strict";

  var component = $("#form-contact"),
      inputs = component.find(".input"),
      required = component.find(".input.required"),
      formInfo = $(".form-info");


  function onFormSubmit() {
    var inputTemp,
        hasError = false;

    required.each(function (idx) {
      inputTemp = required.eq(idx);
      if($.trim(inputTemp.val()) === "") {
        hasError = true;
        inputTemp.addClass("error");
      }
      if (hasError) {
        event.preventDefault();
        formInfo.addClass("error");
      };
    });
  }


  function onInputFocus () {
    var target = $(this);
    if (target.hasClass("error")) {
      target.removeClass("error");
    };
  }


  function bindEvents () {
    component.submit(onFormSubmit);
    required.focus(onInputFocus);
  }


  function setMask() {
    component.find(".dddFone").mask("00");
    component.find(".numberFone").mask("00000-0000", {reverse: true});
    component.find(".dddCel").mask("00");
    component.find(".numberCel").mask("00000-0000", {reverse: true});
  }

  return {
    init: function() {
      setMask();
      bindEvents();
    }
  };
})();
app.contact.init();