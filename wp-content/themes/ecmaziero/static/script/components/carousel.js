app.carousel = (function() {
  "use strict";


  function bindEvents() {
    $("#banner").find(".banner-content").owlCarousel({
      singleItem      : true,
      pagination      : false,
      navigation      : true,
      autoPlay        : 4000,
      slideSpeed      : 800,
      paginationSpeed : 800,
      rewindSpeed     : 1000,
      
      navigationText  : ["",""]
    });

    $("#banner").find(".owl-prev").addClass("ico-slide-left");
    $("#banner").find(".owl-next").addClass("ico-slide-right");
  }

  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.carousel.init();