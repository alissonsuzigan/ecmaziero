app.menu = (function() {
  "use strict";

  var $menu = $("#menu");

  function active(el) {
    el.addClass("active");
    el.find("a").addClass("active");
  }

  function deactive(el) {
    el.removeClass("active");
    el.find("a").removeClass("active");
  }


  function bindEvents() {

    $menu.find(".menu-item-has-children").on("mouseover", function() {
      active($(this));
    });

    $menu.find(".menu-item-has-children").on("mouseout", function() {
      deactive($(this));
    });

    $menu.find(".nolink > a").on("click", function(e) {
      e.preventDefault();
    });
  }


  return {
    init: function() {
      bindEvents();
    }
  };
})();
app.menu.init();