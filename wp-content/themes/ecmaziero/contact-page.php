<?php
/**
 * Template Name: Contato
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

  // vars
  // $pageType = "contact";
  // $pageName = "Contato";
  // include "include/header.php";

  // CONTACT FORM ======================================
  // form vars
  $subject = "Formulário de contato - Escritório Maziero";
  $emailTo = "contato@escritoriomaziero.com.br";
  // $emailTo = "alisson.suzigan@gmail.com";

  $hasError = $emailSent = false;
  $name = $company = $dddFone = $numberFone = $dddCel = $numberCel = $email = $message = "";

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name =       stripslashes(trim($_POST["form-name"]));
    $company =    stripslashes(trim($_POST["company"]));
    $dddFone =    stripslashes(trim($_POST["dddFone"]));
    $numberFone = stripslashes(trim($_POST["numberFone"]));
    $dddCel =     stripslashes(trim($_POST["dddCel"]));
    $numberCel =  stripslashes(trim($_POST["numberCel"]));
    $email =      stripslashes(trim($_POST["email"]));
    $message =    stripslashes(trim($_POST["message"]));
    $pattern  =   "/[\r\n]|Content-Type:|Bcc:|Cc:/i";

    if (preg_match($pattern, $name) || preg_match($pattern, $email) || preg_match($pattern, $message)) {
        die("Header injection detected");
    }

    $emailIsValid = preg_match("/^[^0-9][A-z0-9._%+-]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/", $email);

    if($name && $company && $dddFone && $numberFone && $email) {
      $body =  "<p style='font-size:14px;'>";
      $body .= "<strong>Dados enviados pelo site Escritório Maziero</strong><br><br>";
      $body .= "<strong>Nome: </strong>       $name <br>";
      $body .= "<strong>Empresa: </strong>    $company <br>";
      $body .= "<strong>Telefone: </strong>   ($dddFone) $numberFone <br>";
      $body .= "<strong>Celular: </strong>    ($dddCel) $numberCel <br>";
      $body .= "<strong>e-mail: </strong>     $email <br>";
      $body .= "<strong>message: </strong>    $message <br>";
      $body .= "</p>";

      // headers
      $headers = "MIME-Version: 1.1\r\n";
      $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
      $headers .= "From: Escritorio Maziero | Contato <contato@escritoriomaziero.com.br>\r\n";
      $headers .= "Return-Path: contato@escritoriomaziero.com.br\r\n";

      if (mail($emailTo, $subject, $body, $headers)) {
        $emailSent = true;
        $name = $company = $dddFone = $numberFone = $dddCel = $numberCel = $email = $message = "";
        $_POST["form-name"] = $_POST["company"] = $_POST["dddFone"] = $_POST["numberFone"] = $_POST["dddCel"] = $_POST["numberCel"] = $_POST["email"] = $_POST["message"] = "";
        // echo $body;
      }

    } else {
      $hasError = true;
      // echo "error";
    }
  }


get_header(); ?>

	<main id="main" class="site-main" role="main">


		<?php
  		while ( have_posts() ) : the_post();
  			get_template_part( 'content', 'page' );
  		endwhile;
		?>

    <div class="spacer"></div>

    <div class="contact-main">
      
      <div class="text-content">
          <div class="contact-content">
            <i class="contact-icon contact-icon-tel"></i>
            <span class="contact-text contact-tel"><?php echo get_post_custom_values("contato-fone")[0]; ?></span>
          </div>

          <div class="contact-content">
            <i class="contact-icon contact-icon-email"></i>
            <span class="contact-text contact-email"><?php echo get_post_custom_values("contato-email")[0]; ?></span>
          </div>

          <div class="contact-content">
            <i class="contact-icon contact-icon-local"></i>
            <span class="contact-text contact-end"><?php echo get_post_custom_values("contato-endereco")[0]; ?></span>
          </div>
        </div>


      <div class="form-content">

       <?php if ($emailSent) { ?>
        <div class="form-response success">
          <span class="response-message">Seus dados foram enviados com sucesso</span>
        </div>
      <?php } ?>

      <?php if ($hasError) { ?>
        <div class="form-response error">
          <span class="response-message">Ocorreu um erro no envio do formulário. Tente novamente.</span>
        </div>
      <?php } ?>


        <form id="form-contact" class="form-contact" action="<?php echo $_SERVER['REQUEST_URI']; ?>#form-contact" role="form" method="post">

          <div class="field-line">
            <label for="" class="label">Nome *</label>
            <input type="text" class="input required" name="form-name" value="<?php echo $name;?>">
          </div>
          <div class="field-line">
            <label for="" class="label">Empresa *</label>
            <input type="text" class="input required" name="company" value="<?php echo $company;?>">
          </div>

          <div class="field-line">
            <label for="" class="label">Telefone *</label>
            <div class="field-half">
              <input type="text" class="input required dddFone" name="dddFone" value="<?php echo $dddFone;?>">
              <input type="text" class="input required numberFone" name="numberFone" value="<?php echo $numberFone;?>">
              <label for="" class="label">Cel.:</label>
              <input type="text" class="input dddCel" name="dddCel" value="<?php echo $dddCel;?>">
              <input type="text" class="input numberCel" name="numberCel" value="<?php echo $numberCel;?>">
            </div>
          </div>

          <div class="field-line">
            <label for="" class="label">E-mail *</label>
            <input type="text" class="input required" name="email" value="<?php echo $email;?>">
          </div>

          <div class="field-line">
            <label for="" class="label">Mensagem</label>
            <textarea class="textarea" name="message" rows="7" value="<?php echo $message;?>"></textarea>
          </div>
          <div class="field-line right">
            <input class="reset" type="reset" value="Limpar" >
            <input class="submit" type="submit" value="Enviar">
          </div>
        </form>
        <p class="form-info">* Campo de preenchimento obrigatório.</p>

      </div>
    </div>

	</main>

<?php get_footer(); ?>
