<?php
/**
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title>Escritório Maziero</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php //wp_head(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/static/script/startup.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,400' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/static/style/style.min.css" type="text/css" media="all">
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site-page">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

		<header id="header" class="site-header" role="banner">
			<div class="content-width" role="banner">

				<div class="site-brand">
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<!-- <button class="header-toggle"><?php _e( 'Menu and widgets', 'twentyfifteen' ); ?></button> -->
				</div>
				<?php get_template_part( 'menu' ); ?>

			</div>
		</header><!-- .site-header -->


	  <?php get_template_part( 'banner' ); ?>

		<div id="content" class="site-content"><!-- closed in footer -->
			<div class="content-width"><!-- closed in footer -->
