<?php
/**
 * Template Name: Principal
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="col-70">
  <main id="main" class="site-main" role="main">
    <?php
      // content added by user
      while ( have_posts() ) : the_post();
        get_template_part( 'content', 'page' );
      endwhile;
    ?>
  </main>


  <div class="site-main">
    <h2 class="title">Ferramentas úteis</h2>
    <p>Precisa gerar uma Guia? Formulário? Certidões? Clique em uma das opções abaixo e ganhe tempo com as Ferramentas que nós do Escritório Maziero disponibilizamos para você!</p>

    <div class="useful-tools">

      <ul class="tool-list">
        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Formulário</span>
            <span class="tool-content">
              <i class="icon-tool icon-formulario"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="https://www.awatar.net.br/recibo/NFC/Empresa/EmitirNotaPromissoria.aspx">nota promissória</a></li>
                </ul>
              </span>
            </span>
          </div>
        </li>

        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Gerar Guias</span>
            <span class="tool-content">
              <i class="icon-tool icon-guias"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="http://www.netcontabil.com.br/package_update/conteudo/darf.php">darf</a></li>
                  <li><a target="_blank" href="http://www.netcontabil.com.br/package_update/conteudo/gps.php">gps</a></li>
                  <li><a target="_blank" href="http://www.fazenda.sp.gov.br/guias/demais.asp">GRNE/GARE/GARE-DR</a></li>
                </ul>
              </span>
            </span>
          </div>
        </li>

        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Certidões</span>
            <span class="tool-content">
              <i class="icon-tool icon-certidoes"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="http://www.receita.fazenda.gov.br/Grupo2/Certidoes.htm">receita federal/INSS</a></li>
                  <li><a target="_blank" href="http://www.dividaativa.pge.sp.gov.br/da-ic-web/inicio.do">receita estadual</a></li>
                  <li><a target="_blank" href="http://189.57.39.10:8080/tbw/loginWeb.jsp?execobj=ServicoHome">municipal</a></li>
                  <li><a target="_blank" href="https://webp.caixa.gov.br/cidadao/Crf/FgeCfSCriteriosPesquisa.asp">fgts</a></li>
                </ul>
              </span>
            </span>
          </div>
        </li>
      </ul>

      <ul class="tool-list">
        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Consultas</span>
            <span class="tool-content">
              <i class="icon-tool icon-consultas"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="http://www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/Cnpjreva_Solicitacao.asp">cnpj</a></li>
                  <li><a target="_blank" href="http://www.receita.fazenda.gov.br/Aplicacoes/ATCTA/cpf/ConsultaPublica.asp">cpf</a></li>
                  <li><a target="_blank" href="http://www.sintegra.gov.br/new_bv.html">sintegra - situação fical de fornecedores</a></li>
                  <li><a target="_blank" href="http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuEndereco">cep</a></li>
               </ul>
              </span>
            </span>
          </div>
        </li>

        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Tabelas</span>
            <span class="tool-content">
              <i class="icon-tool icon-tabelas"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="http://www.receita.fazenda.gov.br/Aliquotas/TabProgressivaCalcMens.htm">irpf</a></li>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/inss/">inss</a></li>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/salario-familia/">salário família</a></li>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/simples">simples nacional</a></li>
                </ul>
              </span>
            </span>
          </div>
        </li>

        <li class="tool-item col-1-3">
          <div>
            <span class="tool-title">Índices</span>
            <span class="tool-content">
              <i class="icon-tool icon-indices"></i>
              <span class="tool-text">
                <ul>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/salario-minimo/">salário mín. federal</a></li>
                  <li><a target="_blank" href="http://www.sitecontabil.com.br/tabelas/salarios/minimo-saopaulo.pdf">salário mín. estadual</a></li>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/ufesp/">ufesp</a></li>
                  <li><a target="_blank" href="http://www.contabeis.com.br/tabelas/selic/">selic</a></li>
                </ul>
              </span>
            </span>
          </div>
        </li>
      </ul>

    </div>
  </div>
</div>

<div class="col-30">
  <?php get_template_part('sidebar-home'); ?>
</div>

<?php get_footer(); ?>


