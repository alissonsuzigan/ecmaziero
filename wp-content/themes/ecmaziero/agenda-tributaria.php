<?php
/**
 * Template Name: Agenda Tributária
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


get_header(); ?>

<main id="main" class="site-main" role="main">
  <iframe id="schedule-frame" class="schedule-frame" src="http://www.receita.fazenda.gov.br/mrfb/agenda-tributaria.html" frameborder="0"></iframe>
</main>

<?php get_footer(); ?>
